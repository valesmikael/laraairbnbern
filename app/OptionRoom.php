<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OptionRoom extends Model
{
    //sert pour les injection dans la bdd
    protected $fillable = ['room_id',
                            'option_id'];
}
