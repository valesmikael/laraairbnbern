<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    //sert pour les injection dans la bdd
    protected $fillable = ['country',
                            'city'];

    
                            
}
