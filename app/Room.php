<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    //sert pour les injection dans la bdd
    protected $fillable = [ 'description',
                            'type_id',
                            'area',
                            'sleeping',
                            'price',
                            'datestart',
                            'dateend',
                            'picture1',
                            'picture2',
                            'picture3',
                            'address_id',
                            'user_id'];

    //relation une reservations peut appartenir qu'a une room
    // function leasings(){
    //     return $this->belongsTo(Leasing::class);
    // }
    function leasings(){
        return $this->hasMany(Leasing::class);
    }
    //relation une option peut appartenir à plusieur room
    function options(){
        return $this->belongsToMany(Option::class);
    }

    //relation une adress ne peut apparteir qu'à une seule room
    function address(){
        return $this->belongsTo(Address::class);
    }

    //relation un user ne peut avoir qu'une room
    function user(){
        return $this->belongsTo(User::class);
    }

    //relation un type ne peut avoir qu'une room
    function type(){
        return $this->belongsTo(Type::class);
    }

    // selectionne et enregistre les options pour une nouvelle location
    function addOptions(array $options){
        $this->options()->attach( $options);
    }
}
