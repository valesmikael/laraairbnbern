<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leasing extends Model
{
    //sert pour les injection dans la bdd
    protected $fillable = ['datestart',
                            'dateend',
                            'user_id',
                            'room_id'];

    //relation une option peut appartenir à plusieur room
    function room(){
        return $this->belongsTo(Room::class);
    }

        
}
