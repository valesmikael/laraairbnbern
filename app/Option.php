<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    //sert pour les injection dans la bdd
    protected $fillable = ['label'];
}
