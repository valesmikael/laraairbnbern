<?php

namespace App\Http\Controllers;

use App\Room;
use App\Type;
use App\Option;
use App\Address;
use App\User;
use Auth;
use Illuminate\Http\Request;

class RoomController extends Controller
{
    function __construct(){
        //les trois middleware ont été creer par moi même
        //si l'utilisateur n'est pas authentifié, il ne peut acceder qu'à index et show
        $this->middleware('auth', ['except' => ['index', 'show']]);
        //si l'utilisateur est authentifié en tand que advertiser, il peut creer
        $this->middleware('check.advertiser.room', ['only'=> ['create','store']]);
        //si l'utilisateur est authentifié en tand que l'auteur, il peut acceder a editer, il peut modifier et supprimer
        //$this->middleware('check.author', ['only'=> ['edit','update','destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rooms= Room::all();
        
        return view('rooms.index', compact('rooms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // affiche la vue de creation d'une room
    public function create()
    {
        // intégre les données pour intégrer les type de room
        $types = Type::all();
        // intégre les données pour intégrer les options(equipement) de room
        $options = Option::all();
                
        return view ('rooms.create', compact('types','options'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //Creation d'une room dans la bdd 
    public function store(Request $request)
    {
        //validation des champs
        $fields = $request->validate([
            'country'=>'required',
            'city'=>'required',
            'area'=>'required',
            'description'=>'required|max:300',
            'sleeping'=>'required',
            'price'=>'required|max:10',
            'datestart'=>'required',
            'dateend'=>'required',
            'type_id'=>'required',
            'picture1'=> 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
            'picture2'=> 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
            'picture3'=> 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);
            
        //renomage et redirection pour le stockage des photos        
        $path1 = $request->file('picture1')->store('public/images');
        //on recupère le nom seul de l'image
        $image1 =basename($path1);

        $path2 = $request->file('picture2')->store('public/images');
        $image2 =basename($path2);
        $path3 = $request->file('picture3')->store('public/images');
        $image3 =basename($path3);
        

        //injection du nouveau chemin de path dans le fields
        $fields['picture1']= $image1;
        $fields['picture2']= $image2;
        $fields['picture3']= $image3;

        //ajout de l'user   
        $fields['user_id'] = Auth::user()->id;
        //creation de l'address
        $address = Address::create($fields);
        //ajout de l'address
        $fields['address_id'] = $address->id;       

        // Creation de la room
        $room = Room::create($fields);

        //ajout des options (equipements)
        $room->addOptions( $request['options']);

        //redirection vers la vue
        return redirect(route('rooms.index'));      
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function show(Room $room)
    {
                
        return view('rooms.show',compact('room'));
        
    }

    public function showAdvertiserRoom(Room $room)
    {
        $rooms = $room->whereUserId(Auth::id())->get();
                
        return view('rooms.advertiser',compact('rooms'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function edit(Room $room)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Room $room)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function destroy(Room $room)
    {
        //
    }
}
