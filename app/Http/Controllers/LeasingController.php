<?php

namespace App\Http\Controllers;

use App\leasing;
use App\Room;
use Auth;
use App\User;
use Illuminate\Http\Request;

class LeasingController extends Controller
{
    function __construct(){
        //les trois middleware ont été creer par moi même
        //si l'utilisateur est authentifié, il peut acceder à index, create et store 
        $this->middleware('auth', ['only' => ['create','store','show']]);       
        
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Room $room)
    {
        return view ('leasings.create',compact('room'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Room $room)
    {
        //validation des champs
        $fields = $request->validate([
                                    'datestart'=>'required',
                                    'dateend'=>'required',
                                    ]);
        
        //ajout de l'user
        $fields['user_id'] = Auth::user()->id;

        //ajout de la room
        $fields['room_id'] = $room->id;
        
        //creation de la reservation
        $leasing = Leasing::create($fields);

        //redirection vers la vue
        return redirect(route('rooms.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\leasing  $leasing
     * @return \Illuminate\Http\Response
     */
    public function show(leasing $leasing)
    {
        //
    }

    // récupère mes reservations
    public function showAuthLeasings()
    {
                
        $leasings = Auth::user()->leasings;
                                
        return view('leasings.userLeasings',compact('leasings'));
                
    }

    // affiche mes biens loués
    public function showAuthLeasingsReserved()
        {

        $rooms = Auth::user()->rooms;
                              
               
        // dd($reservations);

        return view('leasings.authLeasingsByUser',compact('rooms'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\leasing  $leasing
     * @return \Illuminate\Http\Response
     */
    public function edit(leasing $leasing)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\leasing  $leasing
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, leasing $leasing)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\leasing  $leasing
     * @return \Illuminate\Http\Response
     */
    public function destroy(leasing $leasing)
    {
        //
    }
}
