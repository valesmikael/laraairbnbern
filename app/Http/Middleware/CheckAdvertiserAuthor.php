<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckAdvertiserAuthor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    // function qui verifie si on est authentifié comme avertiser soit le role 1
    public function handle($request, Closure $next)
    {
        if(Auth::user()->role_id != 1)
        return back();

        return $next($request);
    }
}
