<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckAuthor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

     // function  qui vérifie l'auteur de la room
    public function handle($request, Closure $next)
    {
        
        if(Auth::user()->id != $request->room->user_id)
            return back();
    
        return $next($request);
    }
}
