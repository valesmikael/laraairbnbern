<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
// route vers les biens de l'auth
route::get('/rooms/advertiser', 'RoomController@showAdvertiserRoom')
         ->name('rooms.advertiser');

// ensemble des routes concernant une room
Route::resource('rooms','RoomController')->parameters(['room' => '[0-9]+']);

// route vers mes reservations
route::get('/leasings/userLeasings', 'LeasingController@showAuthLeasings')
         ->name('leasings.userLeasings');

// route vers les biens appartenant a l'auth et qui sont loués
route::get('/leasings/authLeasingsByUser', 'LeasingController@showAuthLeasingsReserved')
         ->name('leasings.authLeasingsByUser');

// ensemble des routes concernant un leasing
Route::resource('rooms/{room}/leasings','leasingController')
                ->parameters([
                            'room' => '[0-9]+',
                            'leasing' => '[0-9]+'
                            ]);

// route qui renvoie vers la page home
Route::get('/', 'HomeController@index')->name('home');


