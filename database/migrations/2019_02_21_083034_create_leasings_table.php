<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeasingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leasings', function (Blueprint $table) {
            $table->increments('id');
            $table->date('datestart');
            $table->date('dateend');
            $table->unsignedInteger('room_id');
            $table->unsignedInteger('user_id');
            $table->timestamps();

            $table->unique(['datestart','dateend','room_id','user_id']);          


            $table->foreign('room_id')
                    ->references('id')
                    ->on('rooms')
                    ->onDelete('restrict');
    
            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('restrict');

            });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leasings');
    }
}
