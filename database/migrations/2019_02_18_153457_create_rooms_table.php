<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('area');
            $table->text('description',300);
            $table->integer('sleeping');
            $table->text('picture1');
            $table->text('picture2');
            $table->text('picture3');
            $table->integer('price');
            $table->date('datestart');
            $table->date('dateend');
            $table->unsignedInteger('address_id');
            $table->unsignedInteger('type_id');
            $table->unsignedInteger('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
