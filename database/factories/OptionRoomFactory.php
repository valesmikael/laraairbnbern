<?php

use Faker\Generator as Faker;

$factory->define(App\OptionRoom::class, function (Faker $faker) {
    return [
        'room_id'=>App\Room::all()->random()->id,
        'option_id'=>App\Option::all()->random()->id,
    ];
});
