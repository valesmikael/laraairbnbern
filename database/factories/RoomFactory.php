<?php

use Faker\Generator as Faker;

$factory->define(App\Room::class, function (Faker $faker) {
    return [
            'description'=>$faker->text(100),
            'type_id'=>App\Type::All()->random()->id,
            'area'=>$faker->randomDigitNotNull,
            'sleeping'=>$faker->numberBetween($min = 1, $max = 10),
            'price'=>$faker->numberBetween($min = 150, $max = 1500),
            'datestart'=>$faker->date(),
            'dateend'=>$faker->date(),
            'picture1'=>$faker->imageUrl($width = 640, $height = 480),
            'picture2'=>$faker->imageUrl($width = 640, $height = 480),
            'picture3'=>$faker->imageUrl($width = 640, $height = 480),
            'address_id'=>App\Address::All()->random()->id,
            'user_id'=>App\User::All()->random()->id,
    ];
});
