etend app.blade.php
@extends('layouts.app')

{{-- ajoute le titre "vos locations " dans l'onglet --}}
@section('title', 'Vos locations')

@section('content')

<div class="d-flex justify-content-around row">
        
    @foreach ($rooms as $room)
        @foreach ($room->leasings as $leasing)            
            <div class="card" style="width: 20rem;">
                <img src="{{$room->picture1}}" class="card-img-top">
            
                    <div class="card-body">
                        <h5 class="card-title">Pays : {{$room->address->country}}</h5>                
                        <h5 class="card-title">Ville : {{$room->address->city}}</h5>            
                    </div>            
                <p class="card-text"><small class="text-muted">
                    Vous avez loué du {{$leasing->datestart}} au {{$leasing->dateend}}</small></p>
                    <a href="{{route('rooms.show', $room)}}" class="btn btn-primary">Détails</a>
        </div> 
        @endforeach      
    @endforeach      
        
    
        
</div>

@endsection 

