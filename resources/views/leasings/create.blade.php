{{-- etend app.blade.php --}}
@extends('layouts.app')

{{-- ajoute le titre "Ajouter un bien " dans l'onglet --}}
@section('title', 'Louer un bien')
 
{{-- creation d'un header --}}
@section('header')

    
@endsection

{{-- creation du contenu --}}
@section('content')

{{-- affichage des erreurs --}}
@include('errors')

{{-- formulaire de location --}}
<div class="card" style="width: 50rem;">
    <form action="{{route('leasings.store',$room)}}" method="POST">

        {{-- champs de protection CSRF --}}
        @csrf

        <h3>Formulaire de reservation :</h3>        
                
        {{-- Date d'arrivé à la location --}}
        <div class="form-group">
            <label> Date d'arrivée</label>
        <input type="date" name="datestart" class="form-control">
        </div>
        
        {{-- Date de départ de la location --}}
        <div class="form-group">
            <label> Date de départ</label>
            <input type="date" name="dateend" class="form-control">
        </div>                   

        <button type="submit" class="btn btn-primary">louer</button>
    </form>
    
</div>
    
@endsection