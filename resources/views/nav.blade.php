<ul class="nav justify-content-center">

        <li class="nav-item">
            <a class="nav-link" href="{{ route('home') }}">{{ __('Acceuil') }}</a> 
        </li>

                    
        <li class="nav-item">
            <a class="nav-link" href="{{ route('rooms.index') }}">{{ __('Chambres') }}</a>           
        </li> 
                     
        
        @auth

        @if (Auth::user()->role_id == 1)
                <li class="nav-item">
                                <a class="nav-link" href="{{ route('rooms.create') }}">{{ __('Creer une location') }}</a>           
                </li>               
                <li class="nav-item">
                        <a class="nav-link" href="{{ route('rooms.advertiser') }}">{{ __('Mes biens') }}</a>           
                </li>
                <li class="nav-item">
                        <a class="nav-link" href="{{ route('leasings.authLeasingsByUser') }}">{{ __('Mes biens loués') }}</a>           
                </li>
        @endif 

        <li class="nav-item">
                <a class="nav-link" href="{{ route('leasings.userLeasings') }}">{{ __('Mes reservations') }}</a>           
        </li>              
                
        @endauth
                   
        
    </ul>

    