{{-- affichage des erreurs si il y en a --}}
@if ($errors->any())
    <div class="errors">
        <ul>
            {{-- boucle sur les type d'erreur --}}
            @foreach ($errors->all() as $error)

            <li>{{ $error }}</li>        

            @endforeach
        </ul>            

    </div>
@endif