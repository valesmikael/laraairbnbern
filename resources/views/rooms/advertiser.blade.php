{{-- etend app.blade.php --}}
@extends('layouts.app')

{{-- ajoute le titre "Ajouter un bien " dans l'onglet --}}
@section('title', 'Vos biens')

@section('content')

<div class="d-flex justify-content-around row">
        
    @foreach ($rooms as $room)            
        <div class="card" style="width: 20rem;">
            <img src="{{$room->picture1}}" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title">Pays : {{$room->address->country}}</h5>                
                    <h5 class="card-title">Ville : {{$room->address->city}}</h5>               
                <a href="{{route('rooms.show', $room)}}" class="btn btn-primary">Détails</a>
                </div>
        </div> 
    @endforeach      
        
</div>

@endsection