
{{-- etend app.blade.php --}}
@extends('layouts.app')

{{-- ajoute le titre "Ajouter un bien " dans l'onglet --}}
@section('title', 'accueil')

@section('content')

<div class="d-flex justify-content-around row">
        
    @foreach ($rooms as $room)            
        <div class="card" style="width: 20rem;">
            
            <img src=" {{asset("storage/images/{$room->picture1}") }}" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title">Pays : {{$room->address->country}}</h5>                
                    <h5 class="card-title">Ville : {{$room->address->city}}</h5>                
                    <p class="card-text">Type de logement : {{$room->type->type}}</p>
                    <p class="card-text">Surface:  {{$room->area}} m²</p>
                    <p class="card-text">Nombre de couchage:  {{$room->sleeping}}</p>
                    <p class="card-text">Descriptif:  {{$room->description}}</p>
                    <p class="card-text">prix à la semaine:  {{$room->price}} €</p>
                <a href="{{route('rooms.show', $room)}}" class="btn btn-primary">Détails</a>
                </div>
        </div> 
    @endforeach      
        
</div>

@endsection