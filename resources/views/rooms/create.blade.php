{{-- etend app.blade.php --}}
@extends('layouts.app')

{{-- ajoute le titre "Ajouter un bien " dans l'onglet --}}
@section('title', 'Ajouter un bien')
 
{{-- creation d'un header --}}
@section('header')

    <h1>Ajouter une Location</h1>
    
@endsection

{{-- creation du contenu --}}
@section('content')

{{-- affichage des erreurs --}}
@include('errors')

{{-- formulaire de creation d'une room --}}
<div class="card" style="width: 50rem;">
    <form action="{{route('rooms.store')}}" method="POST" enctype="multipart/form-data">

        {{-- champs de protection CSRF --}}
        @csrf

        <h3>Creer votre annonce :</h3>

        <div class="form-group">
            <label> Pays </label>
        <input type="text" name="country" class="form-control" placeholder="Saisissez le pays de résidence" value="{{ old('country')}}">
        </div>

        <div class="form-group">
            <label> Ville </label>
        <input type="text" name="city" class="form-control" placeholder="Saisissez la ville de résidence" value="{{old('city')}}">
        </div>

        <div class="form-group">
            <label> Arguments de vente </label>
            <textarea class="form-control" name="description">{{old('description')}}</textarea>
        </div>

        <div class="form-group">
            <label for="exampleFormControlSelect1">Type de logement</label>
                <select class="form-control" name="type_id">
                    @foreach ($types as $type)
                        <option value="{{ $type->id }}">{{ $type->type }}</option>
                    @endforeach                    
                </select>
        </div>

        <div class="form-group">
            <label> Quelle est la superficie du logement en m²</label>            
            <input type="number" class="form-control" step="5" value="30" min="15" max="400" name="area">
        </div>

        <div class="form-group">
            <label> Quelle est le nombre de couchage du logement </label>            
            <input type="number" class="form-control" step="1" value="1" min="1" max="10" name="sleeping">
        </div>
        
        <div class="form-group">
            <label> Tarif à la semaine en euros</label>
            <input type="text" name="price" class="form-control" placeholder="Prix en €">
        </div>

        
        <div class="form-group">
            <label> Début de mise en location</label>
        <input type="date" name="datestart" class="form-control">
        </div>
        
        <div class="form-group">
            <label> Début de fin de mise en location</label>
            <input type="date" name="dateend" class="form-control">
        </div>
        
        <div class="form-group">
            <label> Ajouter des photos</label>            
            <input type="file" name="picture1" class="form-control">
            <input type="file" name="picture2" class="form-control">
            <input type="file" name="picture3" class="form-control">
        </div>
                

        <h4>Equipement</h4>
        
        @foreach ($options as $option)
            <label>
                <span> {{$option->label}} </span>
                <input type="checkbox" value="{{$option->id}}" name="options[]">
            </label><br>     
        @endforeach

        <button type="submit" class="btn btn-primary">proposer le bien</button>
    </form>
    
</div>
    
@endsection