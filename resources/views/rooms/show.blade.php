{{-- etend app.blade.php --}}
@extends('layouts.app')

{{-- ajoute le titre "Ajouter un bien " dans l'onglet --}}
@section('title', 'detailroom')

@section('content')     


        <div class="card mb-3">
            <div class="row no-gutters">
                <div class="col-md-4">                    
                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                              <div class="carousel-item active">
                                <img src="{{asset("storage/images/{$room->picture1}") }}" class="d-block w-100" alt="...">
                              </div>
                              <div class="carousel-item">
                                <img src="{{asset("storage/images/{$room->picture2}") }}" class="d-block w-100" alt="...">
                              </div>
                              <div class="carousel-item">
                                <img src="{{asset("storage/images/{$room->picture3}") }}" class="d-block w-100" alt="...">
                              </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                              <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                              <span class="carousel-control-next-icon" aria-hidden="true"></span>
                              <span class="sr-only">Next</span>
                            </a>
                          </div>
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title">Pays : {{$room->address->country}}</h5>
                        <h5 class="card-title">Ville : {{$room->address->city}}</h5>
                        <p class="card-text">Type de logement : {{$room->type->type}}</p>
                        <p class="card-text">Surface:  {{$room->area}} m²</p>
                        <p class="card-text">Nombre de couchage:  {{$room->sleeping}}</p>
                        <p class="card-text">Descriptif:  {{$room->description}}</p>
                        <p class="card-text">prix à la semaine:  {{$room->price}} €</p>
                        <p class="card-text"><small class="text-muted">
                            Bien en location du {{$room->datestart}} au {{$room->dateend}}</small></p>
                    </div>
                </div>

            </div>
        </div>
        <h3 style='color:#EC5F5F'>Liste des equipements</h3>  
        <ul class=" list-group-horizontal-lg">
            @foreach ($room->options as $option)                        
                <li class="list-group-item">{{$option->label}}</li>
            @endforeach                        
        </ul>
        <a href="{{route('leasings.create', $room)}}" class="btn btn-primary">Louer</a>
        
        

@endsection
